import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { tap, catchError, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  hostName:string = 'https://fakestoreapi.com/'
  private httpOptions: HttpHeaders;
  constructor(
    private http: HttpClient,
    private router: Router,
    private snackBar: MatSnackBar,
    ) {
    this.httpOptions = new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }

  // getAllProducts = () => {
  //   const endPoint = this.hostName+'products';
  //   return this.http.post<any>(endPoint,{
  //     headers: this.httpOptions,
  //   }).pipe(
  //       tap(resp => {
  //           return resp;
  //       }),
  //       catchError(error => throwError(error))
  //   );
  // }

  async getAllProducts(limit?:any): Promise<any> {
    try {
      const params = new HttpParams().set('limit', limit);
      const endPoint = this.hostName+'products';
      const data = await this.http.get(endPoint,{ params }).toPromise();
      return data;
    } catch (error) {
      console.error('There was an error while fetching data', error);
      throw new Error(error ? error.toString() : 'An unknown error occurred');
    }
  }

  async getAllCategories(): Promise<any> {
    try {
      const endPoint = this.hostName+'products/categories';
      const data = await this.http.get(endPoint).toPromise();
      return data;
    } catch (error) {
      console.error('There was an error while fetching data', error);
      throw new Error(error ? error.toString() : 'An unknown error occurred');
    }
  }
}
