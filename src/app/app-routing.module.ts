import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CatalogComponent } from './components/catalog/catalog.component';

const routes: Routes = [
  {
    path:'',
    component: DashboardComponent,
    title:'Dashboard'
  },
  {
    path:'catalog',
    component: CatalogComponent,
    title:'Dashboard'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
