import { Component } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog.service';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent {
  constructor(
    private catalogService: CatalogService
  ){}

  data:any = null;
  categories:any = null;
  ngOnInit(): void {
    this.getAllCategories();
    this.getAllProducts();
  }

  async getAllProducts(){
    try {
      this.data = await this.catalogService.getAllProducts(30);
      console.log(this.data);
    } catch (error) {
      console.error('There was an error while getting the data in the component', error);
    }
  }

  async getAllCategories(){
    try {
      this.categories = await this.catalogService.getAllCategories();
      console.log(this.categories);
    } catch (error) {
      console.error('There was an error while getting the data in the component', error);
    }
  }
}
