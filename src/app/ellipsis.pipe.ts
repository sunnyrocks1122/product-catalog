import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ellipsis'
})
export class EllipsisPipe implements PipeTransform {

  transform(value: string, maxWords: number): string {
    if (!value) {
      return '';
    }

    const words = value.split(/\s+/);
    if (words.length <= maxWords) {
      return value;
    }

    const truncatedWords = words.slice(0, maxWords);
    return `${truncatedWords.join(' ')}...`;
  }

}
